package com.cn.liukaku.common.mapper.maven;

import com.cn.liukaku.common.domain.TbSysUser;
import com.cn.liukaku.common.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.stereotype.Repository;
import tk.mybatis.mymapper.MyMapper;

import java.util.List;

@Repository("tbSysUserMapper")
@CacheNamespace(implementation = RedisCache.class)
public interface TbSysUserMapper extends MyMapper<TbSysUser> {
    int deleteByPrimaryKey(String userCode);

    int insert(TbSysUser record);

    TbSysUser selectByPrimaryKey(String userCode);

    List<TbSysUser> selectAll();

    int updateByPrimaryKey(TbSysUser record);
}