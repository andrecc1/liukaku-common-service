package com.cn.liukaku.common.mapper.maven;

import com.cn.liukaku.common.domain.LiukakuUser;
import com.cn.liukaku.common.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.stereotype.Repository;
import tk.mybatis.mymapper.MyMapper;

import java.util.List;

@Repository("liukakuUserMapper")
@CacheNamespace(implementation = RedisCache.class)
public interface LiukakuUserMapper extends MyMapper<LiukakuUser> {
    int deleteByPrimaryKey(Integer id);

    int insert(LiukakuUser record);

    LiukakuUser selectByPrimaryKey(Integer id);

    List<LiukakuUser> selectAll();

    int updateByPrimaryKey(LiukakuUser record);
}