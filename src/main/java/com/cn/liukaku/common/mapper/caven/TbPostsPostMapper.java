package com.cn.liukaku.common.mapper.caven;


import com.cn.liukaku.common.domain.TbPostsPost;
import com.cn.liukaku.common.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.stereotype.Repository;
import tk.mybatis.mymapper.MyMapper;

import java.util.List;

@Repository("tbPostsPostMapper")
@CacheNamespace(implementation = RedisCache.class)
public interface TbPostsPostMapper  extends MyMapper<TbPostsPost> {
    int deleteByPrimaryKey(String postGuid);

    int insert(TbPostsPost record);

    TbPostsPost selectByPrimaryKey(String postGuid);

    List<TbPostsPost> selectAll();

    int updateByPrimaryKey(TbPostsPost record);
}